<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ccLite
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info">
			<!-- info for the javascripts -->
			<?php printf( esc_html__( 'Theme: %1$s by %2$s.', 'cclite' ), 'cclite', '<a href="http://www.cortexcode.com" rel="designer">Cortex Code</a>' ); ?>
			<br/><span id='wp-information-bridge' style="font-size: 9px; opacity:0.18;"><?php echo get_home_url(); ?></span><br/>			
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
