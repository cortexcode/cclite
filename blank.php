<?php


/*

register@this-page-intentionally-left-blank.org

the URL to your blank page (for example http://www.example.com/path/to/blankpage.html);

http://www.cortexcode.com/blank.html

your website’s title;

Cortex Code

the URL to your website’s home page (for example http://www.example.com), there should exist a link to your blank page;

http://www.cortexcode.com

a short description of your websites’ contents (one short sentence, no advertising);

I'm a web developer. The site is links to my code & references.

the natural language used on your website;

Dutch, and some english in the blog.

your real name and e-mail address.

Robert van Engelen
regs@cortexcode.com

*/


function saynothing(){
  $themedir = get_bloginfo('template_directory');
  $actual_link = $_SERVER[REQUEST_URI] .' ' . $_SERVER[QUERY_STRING];
  $html = <<<PHP
<!-- cortex code -->
<html>
<head>
	<title>This page intentionally left blank.</title>
  <style type="text/css">
  body span   {
    font-size: 15px; color:grey;
    #font-family: "Arial", Arial, sans-serif;
  }
  a {
    font-size: 11px; color:grey;
  }
  a:hover {
    color:white;
  }
  </style>
  <script style="text/javascript" src="$themedir/js/jquery-1.11.3.min.js"></script>
  <link rel="stylesheet" type="text/css" href="$themedir/css/animate.css">
</head>
<body>
<script type="text/javascript">
  setTimeout(function( ) {
    jQuery('#msg').addClass('animated fadeOut');
  }, 2599);
</script>
<a href="http://this-page-intentionally-left-blank.org/whythat.html">
<table width="100%" height="50%">
	<tr>
		<td align="center" valign="middle"><span href="" id="msg">This page intentionally left blank.</span></td>
	</tr>
</table>
</a>
</body>
</html>
PHP;

echo $html;
die;
}
?>
