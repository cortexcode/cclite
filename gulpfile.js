var gulp = require('gulp');
//var gutil = require('gulp-util');
//var ftp = require('gulp-ftp');
var sftp = require('gulp-sftp');
//var shell = require('gulp-shell')
var config = require('./gulp-config');

gulp.task('usage', function() {
    console.log('Usage\ndeployment: \t"gulp deploy:cortex"\n');
});
gulp.task('default', [ 'usage' ]);

gulp.task('deploy:cortex', function () {
    return gulp.src(['build/**', './css/**', 'img/**', 'inc/**', 'js/**', 'languages/**', 'layouts/**', 'lib/**', 'template-parts/**',
                    '*.php', '*.css', '*.png'],
                    { base: './' })
        .pipe(sftp(config.cortexcode));
});
