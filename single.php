<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package ccLite
 */

get_header(); ?>
	
	<div id="primary" class="content-area " >
		<main id="main" class="site-main jumbotron" role="main">		

		<?php while ( have_posts() ) : the_post(); 

				echo '<div id="'.$post->post_name.'" class="page-container" >';
				echo '<h2>'.$post->post_title .'</h2>';
				echo '<div class="container">'.$post->post_content .'</div>';

				?>
				
					</div>

			<?php //get_template_part( 'template-parts/content', 'single' ); ?>

			<?php //the_post_navigation(); ?>

			<?php
				// If comments are open or we have at least one comment, load up the comment template.
				/*if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;*/
			?>

		<?php endwhile; // End of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php //get_sidebar(); ?>
<?php get_footer(); ?>
