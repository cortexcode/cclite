echo "Building ccLite..."
mkdir ../build/ccLiteProd
cp -r ./* ../build/ccLiteProd/
rm -rf ../build/ccLiteProd/.git
rm -rf ../build/ccLiteProd/build
echo "Zipping."
zip -r ../ccLiteProd.zip ../build/ccLiteProd
echo "Cleaning up."
rm -rf ../build/ccLiteProd/
echo "Done."

