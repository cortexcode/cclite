/**
* function $.fn.onVisible runs callback function once the specified element is visible.
* callback: A function to execute at the time when the element is visible.
* example: $(selector).onVisible(callback);
*/
var ticks = 0;
var originalTagline = '';

function isVisible(elem) {
	var docViewTop = $(window).scrollTop(),
			        docViewBottom = docViewTop + $(window).height(),
			        elemTop = $(elem).offset().top,
			        elemBottom = elemTop + $(elem).height()/4;
	var visible = ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
	return visible;
}

function ccLiteViewModel() {
    // Data
    var self = this;

		self.scrollToId = function(data, event) {
		// if this is not the homepage, reload to get there, so we can scroll to the correct place.
		var currentURL = document.location.href;

		var id = event.currentTarget.hash;

		if ( $('.site-branding').text().length < 1 ){
			document.location = '../' + id;
		}

		$(id).animatescroll( 5000,10000);

	};

	self.onNavbarButtonClick = function(data, event) {
		self.scrollToId(data, event);
		self.toggleMobileMenu();
	};

	self.onNavbarButtonMouseOut = function(data, event) {
		var id = event.currentTarget.id;
		$('#'+id).css('background-color', 'black');
	};

	self.hideMobileMenu = function() {
		// if menu is expanded, hide
		if ( $('.nav').first().hasClass('in') ) {
			self.toggleMobileMenu();
		}
		return true;
	};

	self.toggleMobileMenu = function() {
		// if mobile menu toggler is visible
		if ( $("#mobile-menu-toggle").css("display") != "none" ) {
			// click it, thereby hiding the mobile menu again.
			$("#mobile-menu-toggle").click();
		}
		return true;
	};

	self.toggleMobileMenu = function() {
		// if mobile hamburger menu is visible
		if ( $("#mobile-menu-toggle").css("display") != "none" ) {
			$("#mobile-menu-toggle").click();
		}
		return true;
	};

}

$(document).ready(function() {

		originalTagline = $('.site-description').first().text();

		// all hashtag links in header get data-binding click scroll attributes, for scrolling the page.
		$('nav a').each(function(i) {
			$(this).attr('data-bind', "click: onNavbarButtonClick" ); //, event: { mouseover: onNavbarButtonMouseOver, mouseout: onNavbarButtonMouseOut }");
		});
		// collapse mobile menu on content click
		$('#masthead').attr('data-bind', 'click: hideMobileMenu');
		$('#content').attr('data-bind', 'click: hideMobileMenu');

		$('.skip-link').each(function(i) {
			$(this).attr('data-bind', 'click: scrollToId');
		});

		ko.applyBindings(new ccLiteViewModel());

		// Hide the columns, so we can fade them in later
		$('.col-md-4-fadeIn .container').each(function(i) {
			$(this).css('visibility', 'hidden');
		});

		setInterval(function() {

			// set all fadein classes to fade in when in view
				$('.col-md-4-fadeIn .container').each(function(i) {
				var elem = this;
				var visible = isVisible(elem);
					if ( visible && !$(this).hasClass('animated') ) {
						$(this).addClass('fadeIn');
						$(this).addClass('animated');
						$(this).css('visibility', 'visible');

					 }

			});

			// Pulse the title when in view too
				$('.col-md-4-fadeIn h3').each(function(i){
					var elem = this;
					var visible = isVisible(elem);
					if ( visible && !$(this).hasClass('animated') ) {
						$(this).addClass( "pulse" );
						$(this).addClass( "animated" );
					}
			});

			// kill the value in the dom
			if ( ticks == 0 ){
				$('.site-description').text('');
			}

			/* Show navbar
			var scrollTop = $('body').scrollTop();
			if ( scrollTop > $(window).height()-50 ){
				// turn scrollbar visible
				$('#site-navigation').css('display', 'block');
			}*/

			// monitor body scrolltop for changes

			var body = $( "body" );
			if ( body.scrollTop() > 10 ){
					$('.frontpage-navigation').addClass("animated fadeIn");
			}


			// also, after everything has faded in, type the site tagline one by one.
			ticks++;
			if ( ticks > 7 ) {
					var i = ticks-7;
					var str = originalTagline.substr(0, i);
					$('.site-description').text(str);
					setTimeout(function()
					{
						// animate cursor
						if ( !$('#cursor').hasClass('animated') ) {
							$('#cursor').addClass( "animated" );
							$('#cursor').addClass( "fadeOut" );
						}
					}, 108*originalTagline.length+10);
			}


		}, 108);

	});
