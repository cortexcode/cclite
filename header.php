<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ccLite
 */

?>
<?php

	require_once(get_template_directory().'/blank.php');
	$actual_link = $_SERVER[REQUEST_URI] ;
	echo $root;
	$root = get_template_directory_uri();
	if ( stripos($actual_link, 'blank.html') > -1 ){
		saynothing();
		die;
	}
?>


<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>

</head>
<body <?php body_class(); ?> >

<div id="page" class="hfeed site primary-3">
		<?php if ( is_front_page() && is_home() ) { ?>
			<nav id="site-navigation" class="frontpage-navigation main-navigation navbar navbar-fixed-top primary-2" role="navigation">
		<?php } else { ?>
			<nav id="site-navigation" class="main-navigation navbar navbar-fixed-top primary-2 no-navbar-menu" role="navigation">
		<?php } ?>
	<div class="navbar-header">
      <a class="navbar-brand" id="navbar-brand" href=<? echo get_bloginfo('url'); ?>><?php bloginfo( 'name' ); ?></a>
      <?php

			// set the custom walker for the Bootstrap compatible nav menu
			$walker = new Custom_Walker_Nav_Menu;
			$args = array('on_home_page' => true, 'echo' => 0, 'walker' => $walker, 'container_class' => 'container page-scroll', 'menu_class' => 'nav navbar-nav collapse navbar-collapse' );
			$menu = $walker->wp_nav_menu( $args, true );
			$menu_id = $walker->menu_id;

      ?>

		<button id="mobile-menu-toggle" class="btn btn-primary navbar-toggle collapsed navbar-btn" role="button" data-toggle="collapse" data-target="#<?php echo $menu_id; ?>" aria-expanded="false">
			<span class="glyphicon glyphicon-menu-hamburger"></span>
		</button>

    </div>


			<?php

			// echo the menu
			echo $menu;

			?>
	</nav><!-- #site-navigation -->

	<?php if ( is_front_page() && is_home() ) : ?>

		<header id="masthead" class="site-header primary-0 primary-gradient-1 " role="banner">
			<div class="site-branding">
				<img src="<?php echo( get_header_image() ); ?>" alt="<?php echo( get_bloginfo( 'title' ) ); ?>" class="animated rotateIn"/>
			</div>
			<div class="site-branding animated fadeIn delayedAnim">
				<h1 class="site-title default-text"><?php bloginfo( 'name' ); ?></h1>
				<p class="default-text">
				<span class="site-description default-text"><?php bloginfo( 'description' ); ?></span>
				<span id="cursor" class="cursor ">_</span>
			</p>
			</div><!-- .site-branding -->
		<div class="arrow-down animated fadeIn superDelayedAnim">
		<a class="skip-link screen-reader-text default-text" href="#content"><span class="glyphicon glyphicon-triangle-bottom"></span></a>
		</div>

		</header><!-- #masthead -->

	<?php endif; ?>

	<div id="content" class="site-content">
