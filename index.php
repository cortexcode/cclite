<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ccLite
 */
 
get_header(); ?>

	<div id="primary" class="content-area " >
		<main id="main" class="site-main jumbotron" role="main">

		<?php

			// set the custom walker for the Bootstrap compatible nav menu
			$walker = new Custom_Walker_Nav_Menu;
			$defaults = array('walker' => $walker, 'container_class' => 'container page-scroll', 'menu_class' => 'nav navbar-nav', 'echo' => 0 );
			wp_nav_menu( $defaults );

			if ( $walker->ids != null ) :
				foreach( $walker->ids as $id ) {
				$post = get_post($id);

				echo '<div id="'.$post->post_name.'" class="page-container" >';
				echo '<h2>'.$post->post_title .'</h2>';
				echo '<div class="container">'.$post->post_content .'</div>';
				?>
	</div>

<?php
			}

if ( $walker->cats != null ) {
	foreach ($walker->cats as $cat){
	echo '<div id="'.$cat[0].'" class="page-container fadeInCats">';
	echo '<h2>'.$cat[2].'</h2><br>';
	?>

	<div class="row">

	<?php
	// get three latest posts in category
	$my_query = new WP_Query( 'category_name='.$cat[1].'&posts_per_page=3' );
	while ( $my_query->have_posts() ) : $my_query->the_post();
		$do_not_duplicate = $post->ID;

				echo '<div id="'.$post->post_name.'" class="col-md-4 col-md-4-fadeIn">';

					  		$buttonsize = array(150,150);

					  		//echo '<a href="'.get_permalink($post->ID).'">';
							$post_thumbnail_id = get_post_thumbnail_id( $post->ID );
							if ( $post_thumbnail_id > 0 ) {
								echo '<div class="image-circle">';
								echo wp_get_attachment_image( $post_thumbnail_id, $buttonsize );
								echo '</div>';
								}


				echo '<h3>'.$post->post_title .'</h3>';
				echo '<div class="container">'.$post->post_content .'<br><br></div></div>';

	endwhile;

	?>

	</div>

	<?php

	echo '</div>';

	}
}

?>

<br><br>




<?php endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
